memos
=====

MemOS Simple System Primer 

-- This file will document any notes/expecatations/issues regarding
the Primer 2 assignment. 

-- How to create a virtual disk file

      $dd if=/dev/zero of=disk.img bs=1k count=32760

-- If loop/ext2 device does not exist, have a couple of options:

-- 1. Create /dev/loop

      mknod -m 660 /dev/loop0 b 7 0

-- 2. Edit /etc/modules.conf and add the following line.

      options loop max_loop=8
      
      reboot computer

-- 3. Rebuild kernel with loop option enabled

-- 4. cd /usr/src/linux-2.6/;

      make menuconfig

      	File systems--->

      	<*> Second extended fs support
            
            <*> Ext2 extended attributes
            
            <*> Ext2 POSIX Access Control Lists
      
            <*> Ext2 Security Labels
            
            <*> Ext2 execute in place support
            
      make modules;
      
      make modules install;
      
      modprobe ext2
      
      mount disk.img /mnt/C -t ext2 -o loop,offset=32256


msbeard
