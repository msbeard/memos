# How to assembly, link, and run

# Assemble
$ nasm -f elf msg.nasm

# Link
$ ld -o test msg.o

# Run
$ ./test
