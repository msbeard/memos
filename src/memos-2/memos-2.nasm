section .multiboot
       
        ; Multiboot header -- Safe to place this header in 1st page of memory for GRUB */
        align 4
        dd 0x1BADB002 ; Multiboot magic number */
        dd 0x00000003 ; Align modules to 4KB, req. mem size */
                         ; See 'info multiboot' for further info */
        dd 0xE4524FFB ; Checksum */

section .bootstrap_stack
align 4
stack_bottom:
times 16384 db 0
stack_top:

section .text
global _start

_start: 

; To set up a stack, we simply set the esp register to point to the top of
	; our stack (as it grows downwards).
	mov esp, stack_top
 
    push ebx ; push multiboot header
	extern kernel_main
	call kernel_main
 
    cli
    hlt

hang:
jmp hang

