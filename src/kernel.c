#include "multiboot.h"
//from OSDev
char * itoa( int value, char * str, int base )
{
    char * rc;
    char * ptr;
    char * low;
    // Check for supported base.
    if ( base < 2 || base > 36 )
    {
        *str = '\0';
        return str;
    }
    rc = ptr = str;
    // Set '-' for negative decimals.
    if ( value < 0 && base == 10 )
    {
        *ptr++ = '-';
    }
    // Remember where the numbers start.
    low = ptr;
    // The actual conversion.
    do
    {
        // Modulo is negative for negative value. This trick makes abs() unnecessary.
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"[35 + value % base];
        value /= base;
    } while ( value );
    // Terminating the string.
    *ptr-- = '\0';
    // Invert the numbers.
    while ( low < ptr )
    {
        char tmp = *low;
        *low++ = *ptr;
        *ptr-- = tmp;
    }
    return rc;
}
//modified from stackoverflow to allow concatenation
unsigned int k_printf(char *message, unsigned int line, unsigned int startAt) 
{
    char *vidmem = (char *) 0xb8000;
    unsigned int i=0;
    if (startAt==0) { //if we want to start at the beginning of the line number
    i=(line*80*2);
    } else i=startAt; //we want to concatenate onto the last print

    while(*message!=0)
    {
        if(*message=='\n') // check for a new line
        {
            line++;
            i=(line*80*2);
            *message++;
        } else {
            vidmem[i]=*message;
            *message++;
            i++;
            vidmem[i]=7;
            i++;
        };
    };

    return(i);
};
//from osdev
void write_string( int colour, const char *string )
{
    volatile char *video = (volatile char*)0xB8000;
    while( *string != 0 )
    {
        *video++ = *string++;
        *video++ = colour;
    }
}
int kernel_main(multiboot_info_t* minfostruct)
{
   unsigned int video_offset= k_printf("MemOS: Welcome *** System Memory is: ",10,0);
    unsigned long membytes;
    membytes=0;
 /*   multiboot_memory_map_t* mmap;
    for (mmap = (multiboot_memory_map_t*) minfostruct->mmap_addr;
            (unsigned long) mmap < minfostruct->mmap_addr + minfostruct->mmap_length;
            mmap = (multiboot_memory_map_t*) ((unsigned int) mmap
                + mmap->size + 4 )) {

        if (mmap->type == 1) { 
            membytes++;

        }
    } */
    membytes += minfostruct->mem_lower;
    membytes += minfostruct->mem_upper;
    membytes = membytes/1024;
    char numberone[6];
    itoa(membytes,numberone,10);
    video_offset = k_printf(numberone,10,video_offset);
    k_printf("MB",10,video_offset);


    return 0;	
}
