[BITS 16]			;Tells the assembler that its a 16 bit code
;[ORG 0x7C00]		;Origin, tell the assembler that where the code will
					;be in memory after it is been loaded			
					;ORG directive only works in nasm -f bin 
					
global _start	
	
_start:
	MOV AX, 0x0003		;Set video mode
	INT 0x10
	MOV SI, MemosString ;Store string pointer to SI
	CALL PrintString    ;Call print string procedure

						;Detect Upper Memory using 0xE801
	MOV AX, 0xE801		;Magic Number
	INT 0x15			;Call BIOS. Request memory.
						;AX = Number of contiguous Kb, 1M to 16M
						;BX = Contiguous 64 Kb pages above 16 M
	CALL MB				;AX has MB 			
	CALL PrintInteger

	MOV SI, MBString	;Store string pointer to SI
	CALL PrintString	;Call print string procedure
	JMP $				;Infinite loop, hang it here.

; Note: This document helped me out alot!
; www.tutorialspoint.com/assembly_programming/pdf/assembly_arithmetic_instructions.pdf

MB:						;Convert KB to MB (i: AX, BX, o: XXXMB)
	XOR DX, DX			; High order 16 bits are in DX. Zero out
	MOV CX, 1024		;Divisor - 16 bit
	DIV CX				;Divide AX by 1024; Quotient stored in AX
	XOR AH, AH			;Clear AH
	PUSH AX				;Push AX on top of stack - Need to add < 16MB to Rest of memory
	
	MOV AX, BX			;Move BX to AX
	MOV CX, 16			;(AX*64)/1024 == AX/16; 
						;8-bit divisor 
	DIV CX				;Divide AX by 16. Quotient stored in AL
	XOR AH, AH			;Zero out AH
	XOR CX, CX
	MOV CX, AX
	POP DX				;Pop AX off stack and store in DX
	ADD AX, DX			;Sum Memory Contents
						;? Why off by 1? 
						;Ans: Memory is fuzzy according to OSDevWiki
						;Thus Address space = [1-MB-1]?
						;Also, 0xE801 only detects memory above 1MB. 
						;That is why memory is off by 1!
	ADD AX, 1
	RET

PrintInteger:			;Procedure to print an integer on screen
	XOR BX, BX			;Counter
PrintLoop:
	MOV CL, 10			;Divisor
	DIV CL				;Divide EAX by 10
	ADD AH, 48			;Convert remainder into ASCII
	XOR DX, DX			;Prepare DH
	MOV DL, AH			;
	PUSH DX				;Save the number
	XOR AH, AH			;Zero remainder section
	INC BX
	CMP AL, 0x0			;If no quotient, were done
	JZ ProcessPushed
	JMP PrintLoop

ProcessPushed:
	CMP BX, 0x0			;see if anything left on stack
	JZ exit_function
	DEC BX				;Decrease stackcount
	POP AX				;Pop Last Pushed
	MOV AH, 0x0E		;Tell Bios that we need to print
	INT 0x10
	JMP ProcessPushed

PrintCharacter:			;Procedure to print character on screen
						;Assume that ASCII value is in register AL
	MOV AH, 0x0E		;Tell BIOS that we need to print one charater on screen.
	MOV BH, 0x00		;Page no.
	MOV BL, 0x07		;Text attribute 0x07 is lightgrey font on black background
	INT 0x10			;Call video interrupt
	RET					;Return to calling procedure

PrintString:			;Procedure to print string on screen
						;Assume that string starting pointer is in register SI

next_character:			;Label to fetch next character from string
	MOV AL, [SI]		;Get a byte from string and store in AL register
	INC SI				;Increment SI pointer
	OR AL, AL			;Check if value in AL is zero (end of string)
	JZ exit_function 	;If end then return
	CALL PrintCharacter ;Else print the character which is in AL register
	JMP next_character	;Fetch next character from string

exit_function:			;End label
	RET					;Return from procedure

MemosString db 'MemOS: Welcome *** System Memory is: ', 0
MBString db 'MB', 0	
	
TIMES 510 - ($ - $$) db 0	 	;Fill the rest of sector with 0
DW 0xAA55 			 			;Add boot signature at the end of bootloader