section .multiboot
       
        ; Multiboot header -- Safe to place this header in 1st page of memory for GRUB */
        align 4
        dd 0x1BADB002 ; Multiboot magic number */
        dd 0x00000003 ; Align modules to 4KB, req. mem size */
                         ; See 'info multiboot' for further info */
        dd 0xE4524FFB ; Checksum */

section .bootstrap_stack
align 4
stack_bottom:
times 16384 db 0
stack_top:

section .text
global _start

_start: 

; To set up a stack, we simply set the esp register to point to the top of
	; our stack (as it grows downwards).
	mov esp, stack_top
 
	; We are now ready to actually execute C code. We cannot embed that in an
	; assembly file, so we'll create a kernel.c file in a moment. In that file,
	; we'll create a C entry point called kernel_main and call it here.
    push ebx ; push multiboot header
	extern kernel_main
	call kernel_main
 
	; In case the function returns, we'll want to put the computer into an
	; infinite loop. To do that, we use the clear interrupt ('cli') instruction
	; to disable interrupts, the halt instruction ('hlt') to stop the CPU until
	; the next interrupt arrives, and jumping to the halt instruction if it ever
	; continues execution, just to be safe.
    cli
    hlt

hang:
jmp hang

