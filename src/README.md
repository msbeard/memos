memos-2
====


-- In order to do the guide:
    
    qemu-img create -f raw c.img 30M

    the above will create a 30Meg raw disk, now you can proceed

-- You'll get stuck around installing grub, just copy from puppy's
/boot/grub  --

-- compiling nasm to prep for elf:

    nasm -f elf memos-2.nasm -o memoself.o

    compile kernel:

    gcc -c kernel.c -nostdinc -ffreestanding -o kernel.o
    -std=gnu99

--invoke linker script

    ld -T memos-2.ld memoself.o kernel.o -o memos-2.elfbin
 
 --copy it into the /mnt/C/boot/FILENAME, which you specified in your
 menu.1st for grub
 --Note: I think we can do the guide without grub and then copy our
 memos-1 raw binary to the right place -- remember he said offset 0 is
 lucky, not sure where to stick it still
